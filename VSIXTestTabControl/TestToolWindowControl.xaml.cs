﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace VSIXTestTabControl
{
    /// <summary>
    /// Interaction logic for TestToolWindowControl.
    /// </summary>
    public partial class TestToolWindowControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestToolWindowControl"/> class.
        /// </summary>
        public TestToolWindowControl()
        {
            this.InitializeComponent();
        }

        private void TabItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Debug.WriteLine(DragDrop.DoDragDrop((TabItem)sender, new DataObject("TestString"), DragDropEffects.All));
            }
        }

        private void TabItem_Drop(object sender, DragEventArgs e)
        {
            Debug.WriteLine(e.Data.GetData(typeof(string)));
        }
    }
}